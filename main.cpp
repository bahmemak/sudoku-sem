// include necessary libraries
#include <iostream>
#include <array>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <atomic>
#include <chrono>
#include <sstream>
#include <random>
#include <algorithm>
#include <cstring>

// Define the Board type as a 9x9 array of integers
using Board = std::array<std::array<int, 9>, 9>;

// Global board variables for tracking the game state
Board solvedBoard, currentBoard, initialBoard;

// Mutex and condition variable for thread synchronization
std::mutex boardMutex;
std::condition_variable boardChanged;

// Atomic boolean to keep track of game's end state
std::atomic<bool> gameOver(false);

// Global variable to keep track of the number of hints used
int hintsUsed = 0;

// Number of cells removed from the board
int removedCells;

// Constants for scoring based on difficulty and penalties
const int EASY_BASE_SCORE = 1000;
const int MEDIUM_BASE_SCORE = 500;
const int HARD_BASE_SCORE = 250;
const int HINT_PENALTY = 50;
const int TIME_PENALTY_PER_SECOND = 10;

// Enum for defining game difficulty levels
enum class Difficulty {
    Easy,
    Medium,
    Hard
};

// Define color codes for console output
const std::string BGDEFAULT = "\033[49m";
const std::string BGINITIAL = "\033[48;5;240m";
const std::string BGCORRECT = "\033[42m";
const std::string BGINCORRECT = "\033[41m";
const std::string FGDEFAULT = "\033[39m";

// Global variable to store the current game difficulty
Difficulty difficulty;

// Forward declarations of functions used in the game
void inputThread();
void outputThread();
void computingThread();
void generateInitialBoard(Difficulty choosedDifficulty);
void renderBoard();
bool isStringValidSudoku(const std::string& field);
void setInitialBoardFromField(const std::string& field);
bool hasUniqueSolution(Board sudoku);
void displayRules();
std::vector<Board> findAllSolutions(Board& grid);
bool isValidInput(int row, int col);
int calculateUserScore(int timeInSeconds, Difficulty usedDifficulty, int numEmptyCells, int numHintsUsed);

// Main function, entry point of the program
int main(int argc, char* argv[]) {
    // Check if a custom Sudoku field was provided as a command line argument
    bool customFieldProvided = false;

    // If a custom field was provided, check if it's valid and set it as the initial board
    for (int i = 1; i < argc - 1; i++) {
        if (strcmp(argv[i], "-field") == 0) {
            std::string field = argv[i + 1];
            if (isStringValidSudoku(field)) {
                setInitialBoardFromField(field);
                customFieldProvided = true;
                break;
            } else {
                std::cerr << "Invalid Sudoku field provided." << std::endl;
                return 1;
            }
        }
    }

    // If no custom field was provided, display the rules and generate a random board
    if (!customFieldProvided) {
        displayRules();
        int diff;

        if (!(std::cin >> diff) || diff < 1 || diff > 3) {
            std::cerr << "Invalid difficulty selected." << std::endl;
            return 1;
        }

        difficulty = static_cast<Difficulty>(--diff);

        generateInitialBoard(difficulty);
    } else {
        // If a custom field was provided, check if it has a unique solution
        if (!hasUniqueSolution(initialBoard)) {
            std::cerr << "Provided Sudoku field doesn't have a unique solution." << std::endl;
            return 1;
        }
        difficulty = Difficulty::Easy;
        solvedBoard = findAllSolutions(initialBoard)[0];
    }

    currentBoard = initialBoard;

    renderBoard();

    // Start the game threads
    std::thread input(inputThread);
    std::thread computing(computingThread);
    std::thread output(outputThread);

    // Wait for the threads to finish
    input.join();
    computing.join();
    output.join();

    return 0;
}

// Function to handle user input in a separate thread
void inputThread() {
    std::string input;
    int row, col, value;

    // Loop until the game is over
    while (!gameOver) {
        std::getline(std::cin, input);

        // Check if the user wants to quit
        if (input == "q") {
            std::unique_lock<std::mutex> lock(boardMutex);
            gameOver = true;
            boardChanged.notify_all();
            return;
        }

        // Ignore empty input
        if (input.empty()) {
            continue;
        }

        // Parse the input
        std::istringstream iss(input);

        // Check if the input is valid
        if (!(iss >> row >> col)) {
            // Print instructions for invalid input format
            std::cout << R"(Invalid input. Please enter values in format "[row], [column], [value]")";
            if (difficulty == Difficulty::Easy) {
                // Additional instruction for hint usage in Easy difficulty
                std::cout << R"( or "[row], [column], h" to get hint)";
            }
            std::cout << std::endl;
            continue;
        }

        // Lock the board for modification
        std::unique_lock<std::mutex> lock(boardMutex);

        // Check if the cell is already solved
        if (currentBoard[row - 1][col - 1] == solvedBoard[row - 1][col - 1]) {
            std::cout << "This cell is already solved." << std::endl;
            continue;
        }

        // Parse the value or hint command
        if (iss >> value) {
            // Validate the entered value
            if (value < 1 || value > 9 || !isValidInput(row, col)) {
                std::cout << "Invalid input. Please enter numbers in range 1-9." << std::endl;
                continue;
            }
            // Update the board with the entered value
            currentBoard[row - 1][col - 1] = value;
            boardChanged.notify_one();
        } else {
            char symbol;
            iss.clear();
            // Check for hint command
            if (!(iss >> symbol) || symbol != 'h') {
                std::cout << "Invalid input. Please use 'h' for hints." << std::endl;
                continue;
            }

            // Validate hint usage
            if (!isValidInput(row, col) || difficulty != Difficulty::Easy) {
                std::cout << "Hints not available for this cell or difficulty mode." << std::endl;
                continue;
            }

            // Apply hint and update the board
            hintsUsed++;
            currentBoard[row - 1][col - 1] = solvedBoard[row - 1][col - 1];
            boardChanged.notify_one();
        }
    }
}

// Function to handle game logic and state updates in a separate thread
void computingThread() {
    // Start the timer
    auto startTime = std::chrono::steady_clock::now();
    // Set the time limit for the Hard difficulty
    const long timeLimit = 180;

    // Loop until the game is over
    while (!gameOver) {
        std::unique_lock<std::mutex> lock(boardMutex);

        // Calculate elapsed time
        auto currentTime = std::chrono::steady_clock::now();
        auto elapsedSeconds = std::chrono::duration_cast<std::chrono::seconds>(currentTime - startTime).count();

        // Check for time-up condition in Hard difficulty
        if (difficulty == Difficulty::Hard && elapsedSeconds >= timeLimit) {
            std::system("clear");
            std::cout << "Time's up! Game Over." << std::endl;
            std::cout << "Press enter to finish." << std::endl;
            gameOver = true;
            boardChanged.notify_all();
            break;
        }

        // Check if the puzzle is solved
        if (currentBoard == solvedBoard) {
            std::system("clear");
            std::cout << "\nCongratulations! You have solved the puzzle!" << std::endl;
            std::cout << "Your score: " << calculateUserScore(elapsedSeconds, difficulty, removedCells, hintsUsed) << std::endl;
            std::cout << "Press enter to finish." << std::endl;
            gameOver = true;
            boardChanged.notify_all();
            break;
        }

        // Unlock the mutex and wait for a short duration
        lock.unlock();
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
}

// Function to render the game board in a separate thread
void outputThread() {
    std::unique_lock<std::mutex> lock(boardMutex);

    while (!gameOver) {
        // Wait for notification on board change
        boardChanged.wait(lock);
        if (gameOver) break;
        // Render the current state of the board
        renderBoard();
    }
}

// Function to check if a given number placement is safe on the Sudoku board
bool isSafe(const Board& grid, int row, int col, int num) {
//    // Check the row and column for the same number
//    for (int i = 0; i < 9; i++) {
//        if (grid[row][i] == num) {
//            return false;
//        }
//    }
//
//    // Check the 3x3 subgrid for the same number
//    for (int i = 0; i < 9; i++) {
//        if (grid[i][col] == num) {
//            return false;
//        }
//    }

// Check the row and column for the same number
    for (int i = 0; i < 9; i++) {
        if (grid[row][i] == num || grid[i][col] == num) {
            return false;
        }
    }

    // Check the 3x3 subgrid for the same number
    int startRow = row - row % 3;
    int startCol = col - col % 3;
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            if (grid[i + startRow][j + startCol] == num) {
                return false;
            }
        }
    }

    return true;
}

// Recursive function to solve the Sudoku and store all solutions
bool solveSudoku(Board& grid, std::vector<Board>& solutions) {
    int row, col;
    bool foundEmpty = false;

    // Find the first empty cell
    for (row = 0; row < 9; row++) {
        for (col = 0; col < 9; col++) {
            if (grid[row][col] == 0) {
                foundEmpty = true;
                break;
            }
        }
        if (foundEmpty) {
            break;
        }
    }

    // If no empty cell is found, a solution is found
    if (!foundEmpty) {
        solutions.push_back(grid);
        return false; // Backtrack to find more solutions
    }


    // Try placing numbers 1 to 9 in the empty cell
    for (int num = 1; num <= 9; num++) {
        if (isSafe(grid, row, col, num)) {
            grid[row][col] = num;

            solveSudoku(grid, solutions); // Recursively try to solve the remaining puzzle

            grid[row][col] = 0; // Backtrack
        }
    }

    return false; // Backtrack
}

// Function to find all solutions for a given Sudoku grid
std::vector<Board> findAllSolutions(Board& grid) {
    std::vector<Board> solutions;
    solveSudoku(grid, solutions);
    return solutions;
}

// Function to check if a given Sudoku grid has a unique solution
bool hasUniqueSolution(Board sudoku) {
    return findAllSolutions(sudoku).size() == 1;
}

// Function to display the game rules and instructions
void displayRules() {
    std::cout << "\033[2J\033[1;1H"; // Clear the screen
    std::cout << "\033[1;97m"; // Set text color to white

    // Print the game title and rules
    std::cout << "Welcome to Sudoku!" << std::endl;
    std::cout << "------------------" << std::endl;
    std::cout << "Rules:" << std::endl;
    std::cout << "\033[0;36m"; // Set text color to cyan
    std::cout << "1. The game is played on a 9x9 grid, divided into 3x3 subgrids called 'regions'." << std::endl;
    std::cout << "2. The goal is to fill all empty cells with numbers between 1 and 9." << std::endl;
    std::cout << "3. Each row, column, and region must contain only one of each number from 1 to 9." << std::endl;
    std::cout << "4. Some cells have numbers in them already, which cannot be changed." << std::endl;
    std::cout << "5. Enter your answers by specifying the row, column, and the desired number." << std::endl;
    std::cout << "6. To enter a number, type the row number, column number, and the number you want to place, separated by spaces." << std::endl;
    std::cout << "   For example, '4 5 7' will place the number 7 in the 4th row and 5th column." << std::endl;
    std::cout << "7. If you enter an incorrect number, it will be highlighted in red." << std::endl;
    std::cout << "8. To get hint for a cell, type the row number, column number, and 'h', separated by spaces." << std::endl;
    std::cout << "9. To quit the game at any time, enter 'q'." << std::endl << std::endl;
    std::cout << "\033[0m"; // Reset text color

    std::cout << "Select difficulty to start game: " << std::endl;
    std::cout << "\033[1;32m" << "1 - Easy (35 filled cells and hints)" << std::endl;
    std::cout << "\033[1;33m" << "2 - Medium (30 filled cells without hints)" << std::endl;
    std::cout << "\033[1;31m" << "3 - Hard (25 filled cells without hints, 3 minute to solve)" << std::endl;
    std::cout << "\033[0m";
}

// Function to calculate the user's score based on the game difficulty, time taken, number of empty cells, and hints used
int calculateUserScore(int timeInSeconds, Difficulty usedDifficulty, int numEmptyCells, int numHintsUsed) {
    // Calculate the base score based on the difficulty
    int baseScore = 0;
    switch (usedDifficulty) {
        case Difficulty::Easy:
            baseScore = EASY_BASE_SCORE;
            break;
        case Difficulty::Medium:
            baseScore = MEDIUM_BASE_SCORE;
            break;
        case Difficulty::Hard:
            baseScore = HARD_BASE_SCORE;
            break;
        default:
            break;
    }

    // Calculate the time and hint penalties
    int timeScorePenalty = timeInSeconds * TIME_PENALTY_PER_SECOND;
    int hintPenalty = numHintsUsed * HINT_PENALTY;

    int userScore = baseScore - hintPenalty - timeScorePenalty + (numEmptyCells * 10);

    // Ensure that the score is not negative
    if (userScore < 0) {
        userScore = 0;
    }

    return userScore;
}


// Function to check if a given string is a valid Sudoku field
bool isStringValidSudoku(const std::string& field) {
    if (field.length() != 81) return false;
    for (char c : field) {
        if (c < '0' || c > '9') return false;
    }
    return true;
}

// Function to set the initial board from a given string
void setInitialBoardFromField(const std::string& field) {
    for (int i = 0; i < 9; ++i) {
        for (int j = 0; j < 9; ++j) {
            initialBoard[i][j] = field[i * 9 + j] - '0';
        }
    }
}

// Function to check if a given cell is valid for user input
bool isValidInput(int row, int col) {
    return !(row < 1 || row > 9 || col < 1 || col > 9 || initialBoard[row - 1][col - 1] != 0);
}

// Function to render the game board
void renderBoard() {
    std::system("clear");

    std::cout << std::endl;

    // Iterate through each cell and render it with appropriate colors
    for (int i = 0; i < 9; i++) {
        if (i % 3 == 0 && i != 0) {
            std::cout << std::endl;
        }

        for (int j = 0; j < 9; j++) {
            if (j % 3 == 0 && j != 0) std::cout << "   ";

            std::string bgColor = BGDEFAULT;
            if (currentBoard[i][j] != 0) {
                if (currentBoard[i][j] == solvedBoard[i][j]) {
                    bgColor = BGCORRECT;
                } else {
                    bgColor = BGINCORRECT;
                }
            } else if (currentBoard[i][j] == 0 && solvedBoard[i][j] != 0) {
                bgColor = BGINITIAL;
            }

            std::cout << bgColor << " ";
            if (currentBoard[i][j] != 0) {
                std::cout << currentBoard[i][j];
            } else {
                std::cout << "·";
            }
            std::cout << " " << BGDEFAULT << FGDEFAULT;
        }
        std::cout << std::endl;
    }

    std::cout << "Enter row (1-9), column (1-9), and value (1-9), or q to quit: " << std::endl;
}

// Function to remove a specified number of cells from the Sudoku board
int removeCells(Board& sudoku, int cellsToRemove) {
    std::random_device rd; // obtain a random number from hardware
    std::mt19937 gen(rd()); // seed the generator

    // Create an array of all cell indices
    std::array<std::pair<int, int>, 9 * 9> cellIndices;
    int count = 0;
    for (int i = 0; i < 9; ++i) {
        for (int j = 0; j < 9; ++j) {
            cellIndices[count++] = std::make_pair(i, j);
        }
    }

    // Shuffle the array of cell indices
    std::shuffle(cellIndices.begin(), cellIndices.end(), gen);

    int removedCount = 0;

    // Remove cells while ensuring the board still has a unique solution
    for (int i = 0; i < cellsToRemove; ++i) {
        int row = cellIndices[i].first;
        int col = cellIndices[i].second;

        // Store the value of the cell before removing it
        int removedValue = sudoku[row][col];
        sudoku[row][col] = 0;

        // Check if uniqueness can still be maintained
        if (!hasUniqueSolution(sudoku)) {
            sudoku[row][col] = removedValue; // If uniqueness cannot be maintained, backtrack
        } else {
            removedCount++;
        }
    }

    return removedCount;
}

// Function to transpose a given Sudoku grid
void transpose(Board& grid) {
    Board temp{};
    for (int i = 0; i < 9; ++i) {
        for (int j = 0; j < 9; ++j) {
            temp[j][i] = grid[i][j];
        }
    }
    grid = temp;
}

// Function to swap two rows in a given region of a Sudoku grid
void swapRowsInRegion(Board& grid, int region, int row1, int row2) {
    std::swap_ranges(grid[region * 3 + row1].begin(), grid[region * 3 + row1].end(), grid[region * 3 + row2].begin());
}

// Function to swap two columns in a given region of a Sudoku grid
void swapColsInRegion(Board& grid, int region, int col1, int col2) {
    for (int i = 0; i < 9; ++i) {
        std::swap(grid[i][region * 3 + col1], grid[i][region * 3 + col2]);
    }
}

// Function to swap two regions horizontally in a Sudoku grid
void swapRegionsHorizontally(Board& grid, int region1, int region2) {
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 9; ++j) {
            std::swap(grid[region1 * 3 + i][j], grid[region2 * 3 + i][j]);
        }
    }
}

// Function to swap two regions vertically in a Sudoku grid
void swapRegionsVertically(Board& grid, int region1, int region2) {
    for (int i = 0; i < 9; ++i) {
        for (int j = 0; j < 3; ++j) {
            std::swap(grid[i][region1 * 3 + j], grid[i][region2 * 3 + j]);
        }
    }
}

// Function to generate a basic Sudoku field
void generateBasicField() {
    for (int i = 0; i < 9; ++i) {
        for (int j = 0; j < 9; ++j) {
            int value = (i * 3 + j + i / 3) % 9 + 1;
            solvedBoard[i][j] = value;
        }
    }
}

// Function to perform a given number of random permutations on a Sudoku grid
void performRandomPermutations(Board& sudokuGrid, int numPermutations) {
    std::random_device rd;
    std::mt19937 gen(rd());

    std::uniform_int_distribution<> disOperation(0, 4);
    std::uniform_int_distribution<> disRegion(0, 2);

    for (int i = 0; i < numPermutations; ++i) {
        int choice = disOperation(gen);

        switch (choice) {
            case 0:
                transpose(sudokuGrid);
                break;
            case 1:
                swapRowsInRegion(sudokuGrid, disRegion(gen), disRegion(gen), disRegion(gen));
                break;
            case 2:
                swapColsInRegion(sudokuGrid, disRegion(gen), disRegion(gen), disRegion(gen));
                break;
            case 3:
                swapRegionsHorizontally(sudokuGrid, disRegion(gen), disRegion(gen));
                break;
            case 4:
                swapRegionsVertically(sudokuGrid, disRegion(gen), disRegion(gen));
                break;
            default:
                break;
        }
    }
}

// Function to convert a given difficulty to the number of cells to remove
int difficultyToCells(Difficulty difficulty) {
    switch (difficulty) {
        case Difficulty::Easy:
            return 33;
        case Difficulty::Medium:
            return 51;
        case Difficulty::Hard:
            return 56;
        default:
            return -1;
    }
}

// Function to generate a random Sudoku board with a given difficulty
void generateInitialBoard(Difficulty choosedDifficulty) {
    generateBasicField();

    performRandomPermutations(solvedBoard, 1000);

    initialBoard = solvedBoard;

    removedCells = removeCells(initialBoard, difficultyToCells(choosedDifficulty));
}
