# Sudoku Game

This project is a C++ implementation of the classic puzzle game Sudoku. It features a command-line interface and includes various levels of difficulty. The game is designed to be run on a console and provides an interactive experience for solving Sudoku puzzles.

## TEST RUN TO CHECK
- `./sudoku -field 539876412728314965641295738462539871385721649197468253256187394913642687874953106` - prepared field
- `9 8 2` - row, col, value

## Features

- Command-line based Sudoku game
- Three difficulty levels: Easy, Medium, Hard
- Custom Sudoku field input option
- Real-time input validation
- Hint system for Easy difficulty level
- Scoring system based on time and difficulty
- Color-coded console output for game board and messages

## Prerequisites

To compile and run this game, you will need:
- A C++ compiler (C++11 or later)
- Standard C++ libraries

## Installation
1. Clone the repository to your local machine.
2. Compile the code using a C++ compiler.

> `g++ -o sudoku main.cpp`
3. Run the compiled program.
> `./sudoku`

## Usage

- Run the executable file in your terminal.
- Follow the on-screen instructions to start the game.
- You can choose the difficulty level or input a custom Sudoku field.
- Enter your moves in the format: `row column value`.
- Use `row column h` for hints (Easy difficulty only).
- Enter `q` to quit the game at any time.

### Running with Custom Field

The game can be started with a custom Sudoku field by using the `-field` argument followed by the field string. A valid field string consists of 81 characters, each representing a cell in the Sudoku grid, row by row. Use a number (1-9) for pre-filled cells and `0` for empty cells.

Examples:
<details><summary>Valid Sudoku Field</summary>
`./sudoku -field 530070000600195000098000060800060003400803001700020006060000280000419005000080079`
</details>

<details><summary>Invalid Sudoku Field</summary>
`./sudoku -field 53007000060019500009800006A800060003400803001700020006060000280000419005000080079`
</details>

<details><summary>Sudoku Field with More than One Solution</summary>
`./sudoku -field 090243800084000293460002007800300010902807500000406708070021460006700030`
</details>



## Difficulty Levels

The Sudoku game offers three distinct difficulty levels, each varying in complexity and gameplay experience:

### Easy
- **Filled Cells**: 35 cells are pre-filled to give the player a starting point.
- **Hints Available**: Players can request hints for any empty cell. Each hint incurs a score penalty.
- **Ideal for Beginners**: This level is designed for new players or those who want a quick and less challenging game.

### Medium
- **Filled Cells**: 30 cells are pre-filled.
- **No Hints**: Hints are not available at this level, increasing the challenge.
- **Moderate Challenge**: Suitable for players who are familiar with Sudoku and seeking a moderately challenging puzzle.

### Hard
- **Filled Cells**: Only 25 cells are pre-filled, presenting a sparse board.
- **Time Limit**: A time limit of 3 minutes is imposed to solve the puzzle, adding pressure to the gameplay.
- **No Hints**: Like the Medium level, no hints are provided.
- **High Challenge**: This level is meant for experienced players looking for a tough puzzle and a race against time.

## Sudoku Generation Algorithm

The Sudoku puzzles are generated through a combination of basic grid generation, random permutations, and cell removal based on the selected difficulty level. Here's an overview of the process:

### Basic Grid Generation
- The algorithm starts with a basic, valid 9x9 Sudoku grid.
- Each number in the grid is carefully placed to ensure that each row, column, and 3x3 subgrid contains all numbers from 1 to 9.

### Random Permutations
- To randomize the puzzle, various permutations are performed on the basic grid. These include:
   - Transposing the entire grid.
   - Swapping rows within the same region (3x3 subgrid).
   - Swapping columns within the same region.
   - Swapping entire regions either horizontally or vertically.
- A significant number of these permutations are executed to ensure a thoroughly shuffled grid while maintaining its validity.

### Difficulty-Based Cell Removal
- Cells are then removed from the grid to create the puzzle. The number of cells removed depends on the chosen difficulty level:
   - Easy: Fewer cells are removed, leaving more clues on the board.
   - Medium: A balanced number of cells are removed.
   - Hard: More cells are removed, making the puzzle sparser and more challenging.
- During removal, the algorithm ensures that the puzzle has a unique solution.
- Special care is taken to maintain the balance and symmetry of the puzzle to the extent possible.

### Unique Solution Verification
- After cell removal, the algorithm checks to ensure that the puzzle has a unique solution. This is crucial for a fair and enjoyable gameplay experience.
- This verification uses a backtracking algorithm that searches for possible solutions. If more than one solution is found, the cell removal process is adjusted.

